package wrz.util.google;

import java.io.IOException;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.api.client.http.HttpExecuteInterceptor;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpUnsuccessfulResponseHandler;

import android.content.Context;

/**
 * Provide access to Google services given a token acquired elsewhere (i.e.
 * GoogleAuthUtil).
 * 
 * @author William R. Zwicky
 */
public class GoogleTokenCredential implements HttpRequestInitializer {
	Context context;
	private String token;

	public GoogleTokenCredential(Context context, String token) {
		// accountManager = new GoogleAccountManager(context);
		this.context = context;
		this.token = token;
	}

	public void initialize(HttpRequest request) {
		RequestHandler handler = new RequestHandler();
		request.setInterceptor(handler);
		request.setUnsuccessfulResponseHandler(handler);
	}

/*
 * My own copy of getToken with BackOff.
 * If Google's getToken isn't good enough, then convert this into a getToken method.
 * 
	protected String doInBackground_x(Void... params) {
		String token = null;

	    BackOff backOffPolicy = new ExponentialBackOff();
	    long backOffMillis;
	    try {
			backOffMillis = backOffPolicy.nextBackOffMillis();
		} catch (IOException e1) {
			// WTF??
			backOffMillis = BackOff.STOP;					
		}
	    
	    for(;;) {
			try {
				token = GoogleAuthUtil.getToken(requestedAuthContext, accountName, requestedAuthScope);
			} catch (IOException transientEx) {
				// Network or server error, try later
				
				if(backOffMillis == BackOff.STOP) {
					transientFailure = transientEx;
					break;
				}
				else {
					Log.e(TAG, String.format("Transient failure, will sleep %f and try again.", backOffMillis / 1000.0));
					
					try {
						Thread.sleep(backOffMillis);
					} catch (InterruptedException e) {
						// ignore
					}						
					
				    try {
						backOffMillis = backOffPolicy.nextBackOffMillis();
					} catch (IOException e) {
						// WTF??
						backOffMillis = BackOff.STOP;					
					}
				}
			} catch (UserRecoverableAuthException e) {
				// Recover (with e.getIntent())
				Log.e(TAG, "getToken failed; trying to recover due to:", e);
				startActivityForResult(e.getIntent(), SAFR_GETTHETOKEN_RECOVER);
				break;
			} catch (GoogleAuthException authEx) {
				// The call is not ever expected to succeed
				// assuming you have already verified that
				// Google Play services is installed.
				hardFailure = authEx;
				break;
			}
	    }

	    
		return token;
	}
*/
	
	class RequestHandler implements HttpExecuteInterceptor, HttpUnsuccessfulResponseHandler {
		boolean received401;
		
		public void intercept(HttpRequest request) throws IOException {
			request.getHeaders().setAuthorization("Bearer " + token);
			
			//I found this in some source, but above doesn't throw them.
			// } catch (GooglePlayServicesAvailabilityException e) {
			// throw new GooglePlayServicesAvailabilityIOException(e);
			// } catch (UserRecoverableAuthException e) {
			// throw new UserRecoverableAuthIOException(e);
			// } catch (GoogleAuthException e) {
			// throw new GoogleAuthIOException(e);
			// }
		}

		public boolean handleResponse(HttpRequest request, HttpResponse response, boolean supportsRetry) {
			if (response.getStatusCode() == 401 && !received401) {
				received401 = true;
				GoogleAuthUtil.invalidateToken(context, token);
				return true;
			}
			return false;
		}
	}
}
