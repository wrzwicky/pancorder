package wrz.util.google;

import java.io.IOException;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.repackaged.com.google.common.base.Joiner;
import com.google.api.services.drive.DriveScopes;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;



/**
 * Reusable sub-activity that connects to Google, logs in user, requests
 * permission from user, and fetches an access token.
 * <PRE>
 * 	  startActivityForResult(context, GoogleAuthActivity.class)
 * </PRE>
 * <P>
 * Inspired by <a href=
 * "http://www.androiddesignpatterns.com/2013/01/google-play-services-setup.html"
 * >Alex Lockwood's blog entry</a>.
 * 
 * @author William R. Zwicky
 */
public class GoogleAuthActivity extends Activity implements Runnable {
	protected static final String TAG = GoogleAuthActivity.class.getPackage().getName();
	
	
	public static final String KEY_AUTH_CONTEXT = "auth_context";
	public static final String KEY_AUTH_SCOPE   = "auth_scope";
	
	public static final String KEY_ACCOUNT_NAME = AccountManager.KEY_ACCOUNT_NAME;
	public static final String KEY_AUTHTOKEN    = AccountManager.KEY_AUTHTOKEN;

	
	private static final int SAFR_VERIFYPLAYSERVICES_DONE = 2001;
	private static final int SAFR_VERIFYUSERACCOUNT_DONE = 2002;
	private static final int SAFR_GETTHETOKEN_RECOVER = 2003;
	

	// Parameters
	Context requestedAuthContext; //token will only work with this context
	String requestedAuthScope;
	
	/**
	 * Queue-based state machine: Handles the sequencing of the stages by using
	 * Handler.post() to keep every method top-level, as opposed to having
	 * methods directly call each other.
	 */
	private enum SequenceState {
		NONE,
		VERIFY_PLAY_SERIVCES,
		VERIFY_USER_ACCOUNT,
		GET_THE_TOKEN
	};
	
	Handler handler = new Handler();
	private SequenceState sequence_state = SequenceState.NONE;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		TextView tv = new TextView(this);
		tv.setText("Connecting to Google Drive ...");
		tv.setTextSize(20);
		tv.setBackgroundColor(0xc0000000);
		tv.setGravity(Gravity.CENTER);
		setContentView(tv);
		
		requestedAuthContext = getApplicationContext();
		requestedAuthScope = usingOAuth2(DriveScopes.DRIVE_FILE);
	}

	@Override
	protected void onStart() {
		super.onStart();
		runState(SequenceState.VERIFY_PLAY_SERIVCES);
	}
	
	/**
	 * Push the next state onto the message queue.
	 * 
	 * @param newState one from SequenceState
	 */
	private void runState(SequenceState newState) {
		sequence_state = newState;
		handler.post(this);
	}

	/**
	 * Switcher to activate current state.
	 */
	public void run() {
		switch(sequence_state) {
			case VERIFY_PLAY_SERIVCES:
				verifyPlayServices();
				break;
			case VERIFY_USER_ACCOUNT:
				verifyUserAccount();
				break;
			case GET_THE_TOKEN:
				getTheToken();
				break;
				
			default:
				finish();
		}
	}
	
	/**
	 * Switcher to bounce results from startActivityForResult() back to methods.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case SAFR_VERIFYPLAYSERVICES_DONE:
				verifyPlayServices_done(resultCode, data);
				break;
			case SAFR_VERIFYUSERACCOUNT_DONE:
				verifyUserAccount_done(resultCode, data);
				break;
			case SAFR_GETTHETOKEN_RECOVER:
				getTheToken_recover(resultCode, data);
				break;

			default:
				super.onActivityResult(requestCode, resultCode, data);
				break;
		}
	}

	

	/**
	 * 1. Verify that Play Services are installed, up to date, and working. If
	 * not, have user install Play, and (hopefully) resume process.
	 */
	private void verifyPlayServices() {
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (status != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
				GooglePlayServicesUtil.getErrorDialog(status, this, SAFR_VERIFYPLAYSERVICES_DONE).show();
			} else {
				Toast.makeText(this, "This device is not supported.", Toast.LENGTH_LONG).show();
				returnResult(RESULT_CANCELED);
			}
		}
		else
			runState(SequenceState.VERIFY_USER_ACCOUNT);
	}
	
	private void verifyPlayServices_done(int resultCode, Intent data) {
		if(resultCode == RESULT_OK)
			runState(SequenceState.VERIFY_USER_ACCOUNT);
		else
			returnResult(RESULT_CANCELED);
	}

	/**
	 * 2. Verify account is still valid. If not or if missing, ask user to log
	 * in.
	 */
	private void verifyUserAccount() {
		String accountName = GoogleAccountUtils.getAccountName(this);
		if (accountName != null) {
			Account account = GoogleAccountUtils.getGoogleAccountByName(this, accountName);
			if (account != null) {
				runState(SequenceState.GET_THE_TOKEN);
			}
			else {
				// The account has since been removed.
				accountName = null;
			}
		}
		// else the user was not found in the SharedPreferences. Either the
		// application deliberately removed the account, or the
		// application's data has been forcefully erased.
		
		if(accountName == null) {
			Intent pickAccountIntent = AccountPicker.newChooseAccountIntent(null, null,
					new String[] { GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE }, true, null, null, null, null);
			startActivityForResult(pickAccountIntent, SAFR_VERIFYUSERACCOUNT_DONE);
		}
	}

	private void verifyUserAccount_done(int resultCode, Intent data) {
		if(resultCode == RESULT_OK) {
			String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
			GoogleAccountUtils.setAccountName(this, accountName);
			runState(SequenceState.GET_THE_TOKEN);
		}
		else
			returnResult(RESULT_CANCELED);
	}

	/**
	 * 3. Get the access token from Google. May ask user to approve the current
	 * app for the requested scope(s).
	 */
	private void getTheToken() {
		AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
			String accountName = GoogleAccountUtils.getAccountName(GoogleAuthActivity.this);
			Exception transientFailure = null;
			Exception hardFailure = null;
			
			@Override
			protected String doInBackground(Void... params) {
				GoogleAccountCredential cred = new GoogleAccountCredential(requestedAuthContext, requestedAuthScope);
				cred.setSelectedAccountName(accountName);
				
				try {
					return cred.getToken();
				} catch (IOException e) {
					hardFailure = e;
				} catch (GoogleAuthException e) {
					hardFailure = e;
				}
				return null;
			}

			@Override
			protected void onPostExecute(String token) {
				if(transientFailure != null) {
					Log.e(TAG, "Transient failure (try again later) due to:", transientFailure);
					Toast.makeText(getApplicationContext(), "Network failure, please try again.", Toast.LENGTH_LONG).show();
					returnResult(RESULT_CANCELED);
				}
				else if(hardFailure != null) {
					Log.e(TAG, "Unable to acquire token due to:", hardFailure);
					Toast.makeText(getApplicationContext(), "Hard failure; see LogCat.", Toast.LENGTH_LONG).show();
					returnResult(RESULT_CANCELED);
				}
				else if(token != null) {
					Log.i(TAG, "Access token retrieved:" + token);
					// No need for another state.
					returnResult(RESULT_OK, accountName, token);
				}
				// else UserRecoverable
			}

			@Override
			protected void onCancelled() {
				returnResult(RESULT_CANCELED);
			}
		};
		task.execute();		
	}

	private void getTheToken_recover(int resultCode, Intent data) {
		if(resultCode == RESULT_OK)
			getTheToken();
		else
			returnResult(RESULT_CANCELED);
	}

// I got RecoverableEx: NeedPermission; gui then asked me to approve Pancorder.
// no token was returned.
// I had to disable and reable google, then I got a token right away.
// Also need to cache and verify token somewhere (pref. caller).

// Got transient error: IOex "NetworkError", but took 10+ secs.


	/**
	 * Return an activity result code with no data, and end activity.
	 * 
	 * @param result result code
	 */
	private void returnResult(int result) {
		Intent data = new Intent();
		setResult(result, data);
		finish();
	}
	
	/**
	 * Return an activity result code with with a Google access token, and end activity.
	 * 
	 * @param result result code
	 * @param token Google access token
	 */
	private void returnResult(int result, String accountName, String token) {
		Intent data = new Intent();
		data.putExtra(KEY_ACCOUNT_NAME, accountName);
		data.putExtra(KEY_AUTHTOKEN, token);
		setResult(result, data);
		finish();
	}
	
	
	
	/**
	 * Build a scope string from one or more scope specs.
	 * 
	 * @param scopes scope spec(s)
	 * @return string properly formatted for Google auth services
	 */
	public static String usingOAuth2(String... scopes) {
		String scope = "oauth2:" + Joiner.on(' ').join(scopes);
		return scope;
	}
}
