package wrz.pancorder.cloud;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

/**
 * Wrap DriveSyncer in an Android thread.
 *
 * @author William R. Zwicky
 */
public class CloudSyncAdapter extends AbstractThreadedSyncAdapter {
	private static final String TAG = CloudSyncAdapter.class.getSimpleName();

	public CloudSyncAdapter(Context context, boolean autoInitialize) {
		super(context, autoInitialize);
	}

	  @Override
	  public void onPerformSync(Account account, Bundle bundle, String authority,
	      ContentProviderClient provider, SyncResult syncResult) {
		  Log.d(TAG, "starting sync");
//	    DriveSyncer syncer = new DriveSyncer(getContext(), provider, account);
//	    syncer.performSync();
	  }
}
