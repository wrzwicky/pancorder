package wrz.pancorder.cloud;

import android.app.Activity;
import android.os.Bundle;

/**
 * Interface for our Cloud Providers.
 *
 * @author William R. Zwicky
 */
public interface CloudAccountManager {
//	getUploader() etc.
	
	/**
	 * Run authorization sequence, conversing with user as needed. Return
	 * results to receiver's onActivityResult(). Account name and auth token
	 * will be sent to that method; use getters here to fetch them.
	 * 
	 * @param receiver
	 *            activity to receive results
	 * @param requestCode
	 *            value to return to onActivityResult()
	 */
	void authorizeWithGui(Activity receiver, int requestCode);

	/**
	 * Attempt authorization when GUI is not allowed, i.e. from a service. If
	 * failure, notification will be posted which will start up GUI
	 * authorization.
	 * 
	 * @param resumeParameters whatever's needed to tell service to retry this
	 */
	void authorizeWithNotification(int resumeParameters);
	
	/**
	 * @param bundle 'extras' returned by activity initiated by authorizeWithGui()
	 * @return name of account contained in bundle
	 */
	String getAccountName(Bundle bundle);

	/**
	 * @param bundle 'extras' returned by activity initiated by authorizeWithGui()
	 * @return access token contained in bundle
	 */
	String getToken(Bundle bundle);
}
