package wrz.pancorder.cloud;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import wrz.pancorder.R;
import wrz.util.google.GoogleAuthActivity;
import wrz.util.google.GoogleTokenCredential;


/**
 * Manager for accessing Google Drive.
 * <P>
 * NOTE: Project must reference google-play-services_lib.
 *
 * @author William R. Zwicky
 */
public class GoogleDriveCloud implements CloudAccountManager {
	protected static final String TAG = GoogleAuthActivity.class.getPackage().getName();

	private Context context;

	public GoogleDriveCloud(Context context) {
		this.context = context.getApplicationContext();
	}

	public String getAccountName(Bundle bundle) {
		// Key name must be sync'd with GoogleAuthActivity.returnResult().
		return bundle.getString(GoogleAuthActivity.KEY_ACCOUNT_NAME);
	}
	
	public String getToken(Bundle bundle) {
		return bundle.getString(GoogleAuthActivity.KEY_AUTHTOKEN);
	}

	public void authorizeWithGui(Activity receiver, int requestCode) {
		Intent intent = new Intent(context, GoogleAuthActivity.class);
		receiver.startActivityForResult(intent, requestCode);
	}

	public void authorizeWithNotification(int resumeParameters) {
		// TODO
/*
		if (e instanceof UserRecoverableAuthException) {
            UserRecoverableAuthException exception = (UserRecoverableAuthException) e;
(NotificationManager) mContext
                .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent authorizationIntent = exception.getIntent();
            authorizationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(
                Intent.FLAG_FROM_BACKGROUND);
            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0,
                authorizationIntent, 0);
            Notification notification = new Notification.Builder(mContext)
                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setTicker("Permission requested")
                .setContentTitle("Permission requested")
                .setContentText("for account " + mAccount.name)
                .setContentIntent(pendingIntent).setAutoCancel(true).build();
            notificationManager.notify(0, notification);
        }
*/
	}
	
	public List<String> getPaths(String token) throws IOException, GoogleAuthException {
		GoogleTokenCredential credential = new GoogleTokenCredential(context, token);
		Drive drive = new Drive.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), credential)
			.setApplicationName(context.getResources().getString(R.string.app_name))
			.build();
		
		ArrayList<String> names = new ArrayList<String>();

		try {
			List<File> files = drive.files().list()
					.setQ("mimeType = 'application/vnd.google-apps.folder' and trashed=false")
					.setMaxResults(99999)
					.execute().getItems();
			for(File file : files) {
				names.add(file.getTitle());
			}
		}
		catch(GoogleJsonResponseException e) {
			// Only sposta be done for 401, but we'll do it for all and hope.
			GoogleAuthUtil.invalidateToken(context, token);
			throw new IOException("Auth failed, try again.");
		}
		
		return names;
	}
}



/*
private void saveFileToDrive() {
    Thread t = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          // File's binary content
          java.io.File fileContent = new java.io.File(fileUri.getPath());
          FileContent mediaContent = new FileContent("image/jpeg", fileContent);

          // File's metadata.
          File body = new File();
          body.setTitle(fileContent.getName());
          body.setMimeType("image/jpeg");

          File file = service.files().insert(body, mediaContent).execute();
          if (file != null) {
            showToast("Photo uploaded: " + file.getTitle());
            startCameraIntent();
          }
        } catch (UserRecoverableAuthIOException e) {
          startActivityForResult(e.getIntent(), REQUEST_AUTHORIZATION);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });
    t.start();
  }
*/
