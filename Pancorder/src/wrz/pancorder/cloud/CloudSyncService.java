// Copyright 2012 Google Inc. All Rights Reserved.

package wrz.pancorder.cloud;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Wrap DriveSyncAdapter in an Android service.
 */
public class CloudSyncService extends Service {

	private static final Object sSyncAdapterLock = new Object();

	private static CloudSyncAdapter sSyncAdapter = null;

	@Override
	public void onCreate() {
		synchronized (sSyncAdapterLock) {
			if (sSyncAdapter == null) {
				sSyncAdapter = new CloudSyncAdapter(getApplicationContext(), true);
			}
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return sSyncAdapter.getSyncAdapterBinder();
	}
}
