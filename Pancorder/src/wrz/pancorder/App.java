package wrz.pancorder;

import java.io.File;
import java.io.IOException;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Environment;
import android.preference.PreferenceManager;

/**
 * Wrapper for app-level utils and parameters.
 * Sets up ACRA to auto-report crashes to me.
 *
 * @author William R. Zwicky
 */
@ReportsCrashes(formKey = "dGJxenZnUFJxX1Y0ZnhDSTVKTUMzU0E6MQ")
public class App extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		ACRA.init(this);
		
		initPrefs();
	}
	
	/**
	 * Install reasonable defaults for app preferences.
	 */
	void initPrefs() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		Editor prefse = prefs.edit();
		
		String local_dev = prefs.getString("local_dev", null);
		if(local_dev == null || local_dev.length() == 0) {
			local_dev = getDefaultfRecordingDir(this).getAbsolutePath();
			prefse.putString("local_dev", local_dev);
		}
		
		String remote_dev = prefs.getString("remote_dev", null);
		if(remote_dev == null || remote_dev.length() == 0) {
			remote_dev = "none";
			prefse.putString("remote_dev", remote_dev);
		}
		
		String remote_path = prefs.getString("remote_path", null);
		if(remote_path == null || remote_path.length() == 0) {
			remote_path = "/Recordings/"+getPackageName();
			prefse.putString("remote_path", remote_path);
		}
		
		prefse.commit();

		//Could use this, but Andorid XML syntax is not powerful enough.
		//PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
	}
	
	/**
	 * @param context
	 *            object that lets us access application info
	 * @return directory where our recordings shall go, or null if no place is
	 *         available
	 */
	public static File getRecordingDir(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		File dir;
		
		String s = prefs.getString("local_dev", null);
		if(s != null)
			dir = new File(s);
		else
			dir = getDefaultfRecordingDir(context);
		return dir;
	}

	/**
	 * Determine a default path for recordings in case there is no preference set.
	 * 
	 * @param context
	 *            object that lets us access application info
	 * @return directory where our recordings go, or null if no place is
	 *         available
	 */
	public static File getDefaultfRecordingDir(Context context) {
		File dir = null;
		
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) == true)
			dir = Environment.getExternalStorageDirectory();
		
		if(dir == null) {
			// Hack to find storage dir.
			// I've seen one phone where this was considered "USB Storage".
			dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getParentFile();
		}
		
		if(dir == null)
			return null;
		
		dir = new File(dir, context.getString(R.string.recording_dir));
		dir = new File(dir, context.getApplicationInfo().packageName);
		
		if(dir.exists())
			; //done!
		else {
			// Create dir, hide it from Android media scanner.
			dir.mkdirs();
			File nomedia = new File(dir, ".nomedia");
			try {
				nomedia.createNewFile();
			} catch (IOException e) {
				//don't care
			}
		}
		return dir;
	}
}
