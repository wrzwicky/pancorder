/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//Omnicorder is a tool that provides real-time data on location, bearing, orientation, light level, magnetic field strength, temperature, air pressure and humidity, subject to the sensors available on your device.

package wrz.pancorder;

import java.util.ArrayList;
import java.util.List;

import wrz.pancorder.recorder.Recording;
import wrz.pancorder.recorder.Recordings;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


/**
 * Main activity.
 * 
 * @author William R. Zwicky
 */
public class RecordingsActivity extends Activity implements RecordingListDelegate {
	private RecordingListAdapter lvAdapter;
	private ListView lvRecordings;

	private Recordings db;
	AsyncTask<Void, Void, Void> refresher;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.recordings);

		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			; // yay!
		}
		else if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
			Toast.makeText(this, "Podcast storage is present but not writable.", Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		else {
			Toast.makeText(this, "Can't access podcast storage.  Is SD card present?", Toast.LENGTH_LONG).show();
			finish();
			return;
		}

		// Poke service to make sure it's still running.
		RecorderServiceCmd.restart(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		lvRecordings = (ListView) this.findViewById(R.id.lvRecordings);
		loadListView(lvRecordings);
		
		refresher = new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				while(!isCancelled()) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
					publishProgress();
				}
				return null;
			}
			
			@Override
			protected void onProgressUpdate(Void... values) {
				lvRecordings.invalidateViews();
			}
		};
		refresher.execute();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		refresher.cancel(true);
		refresher = null;
	}

	protected void loadListView(ListView lvRecordings) {
		db = new Recordings(App.getRecordingDir(this));

		ArrayList<Recording> recs = new ArrayList<Recording>();
		
		// Get active recording, which also cleans up dir for us.
		Recording active = db.getActive();
		recs.add(active);

		// Get metadata for all other recordings.
		recs.addAll(db.getClosed());

		lvAdapter = new RecordingListAdapter(this, recs);
		lvAdapter.setDelegate(this);
		lvRecordings.setAdapter(lvAdapter);
		
		// Activate GUI.
		lvRecordings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Recording rec = (Recording) parent.getItemAtPosition(position);
				if(rec == null)
					startRecording();
				else if(rec.isActive())
					stopRecording(rec);
				// else playRecording
			}
		});

		// Measure recordings (can take a while).
		new AsyncTask<Object, Void, Void>() {
			@Override
			protected Void doInBackground(Object... params) {
//TODO fix race condition!!
				@SuppressWarnings("unchecked")
				List<Recording> recs = (List<Recording>) params[0];
				for(Recording r : recs) {
					if(r != null)
						db.load(r);
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				lvAdapter.notifyDataSetChanged();
			}
		}.execute(recs);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(Menu.NONE, 0, 0, getResources().getString(R.string.menu_prefs));
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case 0:
			startActivity(new Intent(this, PrefsActivity.class));
			return true;
		}
		return false;
	}
	
	public Recording startRecording() {
		Recording rec = db.create();
		lvAdapter.remove(null);
		lvAdapter.insert(rec, 0);
		lvAdapter.notifyDataSetChanged();
		
		rec.setActive(true);
		RecorderServiceCmd.start(this, rec);
		
		return rec;
	}

	public void stopRecording(Recording rec) {
		RecorderServiceCmd.stop(this);
		rec.setActive(false);
		rec.findSpan();
		lvAdapter.insert(null, 0);
		lvAdapter.notifyDataSetChanged();
	}
}
