package wrz.pancorder;

import java.io.File;
import java.io.FileNotFoundException;

import wrz.pancorder.recorder.AudioRecorder;
import wrz.pancorder.recorder.Recording;
import wrz.pancorder.recorder.Recordings;
import wrz.pancorder.recorder.SpaceRecorder;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;



//TODO move *Recorder to thread so main thread doesn't hang on build/tear-down


/**
 * Android Service to record in the background. Use context.startService(intent)
 * to start, and stopService to end recording.
 * 
 * @author William R. Zwicky
 */
public class RecorderService extends Service {
//	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);
	private static final String TAG = RecorderService.class.getPackage().getName();
    private static final int NOTE_RECORDING = 1;
    
    private static RecorderService sInstance = null;
    
    Notification mNoteRecording;

    private Recordings db;
	private AudioRecorder mAudRecorder;
	private SpaceRecorder mSpcRecorder;
	// Length of recording when start() is called.
	private long mSoFar = 0;
	

	/**
	 * Called on system start, and just before onStartCommand.
	 */
	@Override
	public void onCreate() {
		super.onCreate();

		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			; // yay!
		}
		else {
			stopSelf();
			return;
		}
		
		if(sInstance != null)
			throw new IllegalStateException("Service is already running; who's trying to start it again?!");
		sInstance = this;
		
		db = new Recordings(App.getRecordingDir(this));
	}

	/**
	 * Someone is stopping this service without stopping the recording. So we'll
	 * be nice and shut down the recorder, but leave it flagged for restart.
	 */
	@Override
	public void onDestroy() {
		stopRecording();
		sInstance = null;
		super.onDestroy();
	}
    
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	/**
	 * Note this is generally called on the caller's thread.
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//Don't care about flags:
		//  flags == START_FLAG_REDELIVERY doesn't happen; we don't ask for redelivery
		//  flags == START_FLAG_RETRY shouldn't matter
		
		String command = null;
		File file = null;

		// Parse request.
		if(intent == null) {
			// This may happend due to START_STICKY.
			command = "restart";
		}
		else {
			command = intent.getExtras().getString("command");
			file = (File) intent.getExtras().getSerializable("file");
			if(command == null)
				command = "";
		}

		// Implement request.
		if(command.equals("restart")) {
			restartRecording();
		}
		else if(command.equals("start")) {
			file.mkdirs();
			try {
				Recording r = db.get(file);
				startRecording(r);  // no need to check; system will manage
			} catch (FileNotFoundException e) {
				// impossible
			}
		}
		else if(command.equals("stop")) {
			stopRecording();
		}
		else {
			Log.e(TAG, "Received bad command: " + command + " " + file);
		}
    	
    	// if service is killed, system should restart.  will also call onStartCommand(null).
		return START_STICKY;
	}

	/**
	 * Start/continue recording into given target, inform user.
	 * 
	 * @return true if started, false if failed
	 */
	protected boolean startRecording(Recording target) {
		if(mAudRecorder != null && mAudRecorder.getTarget().getDir().equals(target.getDir()))
			return true;
		else {
			stopRecording();
			
	    	showNotification();
	    	
	    	target.findSpan();
	    	mSoFar = target.getDuration();
	    	
	    	mAudRecorder = new AudioRecorder(target);
	    	try {
				mAudRecorder.start();
			} catch (Exception e) {
				Toast.makeText(this, "Unable to start recording due to "+e.toString(), Toast.LENGTH_LONG).show();
				Log.e(TAG, "Exception preparing AudioRecorder", e);
				stopRecording();
		    	return false;
			}
	    	
//	    	mSpcRecorder = new SpaceRecorder(getApplicationContext(), target);
//	    	try {
//				mSpcRecorder.start();
//			} catch (IOException e) {
//				Log.e(TAG, "Exception preparing SpaceRecorder", e);
//			}
	    	
	    	return true;
		}
	}
	
	/**
	 * stop recording, close files
	 */
	protected void stopRecording() {
    	if(mSpcRecorder != null) {
	    	mSpcRecorder.stop();
	    	mSpcRecorder = null;
    	}
    	if(mAudRecorder != null) {
	    	mAudRecorder.stop();
	    	mAudRecorder = null;
    	}
    	clearNotifcation();
	}
	
	/**
	 * check if should be recording, and resume if appropriate
	 */
	protected void restartRecording() {
		Recording active = db.getActive();
		if(active != null) {
			startRecording(active);
		}
	}
	
    /**
     * Show a notification while this service is running.
     */
    private void showNotification() {
        // Set the icon, scrolling text and timestamp
        if(mNoteRecording == null) {
            CharSequence tickerText = "RecorderService is running";
            int icon = R.drawable.ic_launcher;

        	mNoteRecording = new Notification(icon, tickerText, System.currentTimeMillis());
	        mNoteRecording.flags |= Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;
//	        | Notification.FLAG_SHOW_LIGHTS;
//	        mNoteRecording.ledARGB = 0xffff0000;
//	        mNoteRecording.ledOnMS = 333;
//	        mNoteRecording.ledOffMS = 333;
	        
	        // The PendingIntent to launch our activity if the user selects this notification
	        PendingIntent noteIntent = PendingIntent.getActivity(this, 0,
	        			new Intent(this, RecordingsActivity.class), 0);

	        // Set the info for the views that show in the notification panel.
	        mNoteRecording.setLatestEventInfo(this, getText(R.string.app_name), tickerText, noteIntent);
        }
        
        // Send the notification.
        startForeground(NOTE_RECORDING, mNoteRecording);
    }

    private void clearNotifcation() {
    	stopForeground(true);
    }
    
    public long getTotalLength() {
    	return mSoFar + (mAudRecorder == null ? 0 : mAudRecorder.getTotalLength());
    }
    
    /**
     * Nifty hack to access service from an Activity -- just get the instance and call it.
     * 
     * @return most recent instance started by Android system, or null if not running.
     */
    static public RecorderService getInstance() {
    	return sInstance;
    }
}

