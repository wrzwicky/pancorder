package wrz.pancorder;

import wrz.pancorder.recorder.Recording;


/**
 * Callbacks for record/stop/play buttons in list view elements.
 *
 * @author William R. Zwicky
 */
public interface RecordingListDelegate {
	Recording startRecording();
	void stopRecording(Recording rec);
}
