package wrz.pancorder;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import wrz.pancorder.recorder.Recording;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Displays a series of Recording objects in a ListView. Note first object
 * should be the active recording. If none active, first object should be null
 * as placeholder for 'start new'.
 * 
 * @author William R. Zwicky
 */
public class RecordingListAdapter extends ArrayAdapter<Recording> {
	private DateFormat startFmt = DateFormat.getDateInstance(DateFormat.FULL);
	private DateFormat timeFmt = DateFormat.getTimeInstance(DateFormat.SHORT);
	private RecordingListDelegate delegate;
	
 	public RecordingListAdapter(Context context, Recording[] recs) {
		super(context, 0, recs);
	}
	
	public RecordingListAdapter(Context context, ArrayList<Recording> recs) {
		super(context, 0, recs);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Activity activity = (Activity) getContext();
		LayoutInflater inflater = activity.getLayoutInflater();
		Recording rec = getItem(position);
		View view = null;
		
//TODO if convertView != null, reuse it!
		if(rec == null) {
			view = inflater.inflate(R.layout.lvrecordings_new, null);
		}
		else {
			view = inflater.inflate(R.layout.lvrecordings_element, null);
			
			TextView tvStart = (TextView) view.findViewById(R.id.tvStart);
			TextView tvLength = (TextView) view.findViewById(R.id.tvLength);

			StringBuilder buf = new StringBuilder();

			if(rec.isActive()) {
				tvStart.setText("Stop this recording");
//				buf.append(" - (now)");
				buf.append("Recorded: ");
				if(rec.getStart() > 0) {
					buf.append(startFmt.format(new Date(rec.getStart())));
					buf.append(' ');
				}
				buf.append("+");
				buf.append(briefTime(RecorderServiceCmd.getTotalLength()));
			}
			else {
				tvStart.setText(startFmt.format(new Date(rec.getStart())));
				if(rec.getStart() > 0) {
					buf.append(timeFmt.format(rec.getStart()));
					if(rec.getDuration() > 0) {
						buf.append(" - ");
						buf.append(timeFmt.format(rec.getStart()+rec.getDuration()));
					}
				}
			}
			
			tvLength.setText(buf);
		}
		
		return view;
	}

	public void setDelegate(RecordingListDelegate delegate) {
		this.delegate = delegate;
	}
	
	protected void bFunction_onClick_start(View v) {
		Recording rec = delegate.startRecording();
		if(rec != null) {
			ImageButton bFunction = (ImageButton) v;
			bFunction.setTag(rec);
			bFunction.setImageResource(R.drawable.stop);
			bFunction.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					bFunction_onClick_stop(v);
				}
			});
		}
	}
	
	protected void bFunction_onClick_stop(View v) {
		ImageButton bFunction = (ImageButton) v;
		Recording rec = (Recording) bFunction.getTag();
		delegate.stopRecording(rec);
		
		bFunction.setImageResource(R.drawable.play);
		bFunction.setOnClickListener(null);
	}
	
	public static String briefTime(long milliseconds) {
		double t = milliseconds/1000.0;
		if(Math.ceil(t) < 120)
			return String.format(Locale.getDefault(), "%.0fs", Math.ceil(t));
		else {
			t /= 60;
			if(t < 120)
				return String.format(Locale.getDefault(), "%.1fm", Math.ceil(t*10)/10);
			else {
				t /= 60;
				return String.format(Locale.getDefault(), "%.1fh", Math.ceil(t*10)/10);
			}
		}
	}
}
