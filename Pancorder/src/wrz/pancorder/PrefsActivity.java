package wrz.pancorder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.UserRecoverableAuthException;

import android.accounts.Account;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;

import wrz.util.google.GoogleAccountUtils;

import wrz.pancorder.cloud.CloudAccountManager;
import wrz.pancorder.cloud.GoogleDriveCloud;
import wrz.util.FileUtil;

/**
 * Display app prefs screen.
 * Prefs are defined in res/xml/preferences.xml .
 * Note that KEY/VAL constants here must match definitions in that file.
 *
 * @author William R. Zwicky
 */
public class PrefsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
	public static final String KEY_LOCAL_DEV = "local_dev";
	public static final String KEY_REMOTE_DEV = "remote_dev";
	public static final String VAL_REMOTE_DEV_NONE = "none";
	public static final String VAL_REMOTE_DEV_GOOGLE_DRIVE = "google_drive";
	public static final String KEY_REMOTE_PATH = "remote_path";
	public static final String KEY_CREDENTIAL_NAME = "remote_cred_name";

    private static final int SAFR_CHOOSEACCOUNT_DONE = 1001;
	
	private SharedPreferences mPreferences;
	private ListPreference prefLocalDev;
	private ListPreference prefRemoteDev;
	private EditTextPreference prefRemotePath;

	private CloudAccountManager accountMgr;
	private String authToken;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);

        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		prefLocalDev = (ListPreference) findPreference(KEY_LOCAL_DEV);
        prefRemoteDev = (ListPreference) findPreference(KEY_REMOTE_DEV);
        prefRemotePath = (EditTextPreference) findPreference(KEY_REMOTE_PATH);
		
		populateLocalDev(prefLocalDev);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		//TODO prep accountMgr on activity startup
		
		updateSummary_local_dev();
		updateSummary_remote_dev();
		updateSummary_remote_path();
		
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	/**
	 * Called when user changes an entry. Entry will have been accepted and
	 * saved to SharedPrefs.
	 */
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(KEY_LOCAL_DEV)) {
			updateSummary_local_dev();
        }
        else if(key.equals(KEY_REMOTE_DEV)) {
        	chooseAccount();
        }
        else if(key.equals(KEY_REMOTE_PATH)) {
        	updateSummary_remote_path();
        }
    }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case SAFR_CHOOSEACCOUNT_DONE:
				chooseAccount_done(resultCode, data);
				break;
		}
	}
    
	/**
	 * Load available path options into the local_dev Preference.
	 * 
	 * @param lp object to load into
	 */
	void populateLocalDev(ListPreference lp) {
		// Fetch value of the sub-path to append to each of the dir options we find below.
		String subdir = getResources().getString(R.string.recording_dir);
		
		ArrayList<String> keys = new ArrayList<String>();
		ArrayList<String> labels = new ArrayList<String>();

		// Load default path.
		File d = this.getApplication().getFilesDir();
		if(d != null && d.exists()) {
			keys.add(d.getAbsolutePath());
			labels.add("Application Storage:\n"+d);
		}
		
		// Load system "external storage public" path.
		d = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
		if(d != null) {
			d = new File(d.getParentFile(), subdir);
			d = new File(d, getApplication().getPackageName());
			if(!keys.contains(d.getAbsolutePath())) {
				keys.add(d.getAbsolutePath());
				labels.add("Public Storage:\n"+d);
			}
		}
		
		// Load system "external storage" path.
		d = Environment.getExternalStorageDirectory();
		if (d != null) {
			d = new File(d, subdir);
			d = new File(d, getApplication().getPackageName());
			if (!keys.contains(d.getAbsolutePath())) {
				keys.add(d.getAbsolutePath());
				labels.add("External Storage:\n" + d);
			}
		}

		String p;
		
		// Load all external mounts available.
		for(String pp : FileUtil.getExternalMounts()) {
			if(pp != null) {
				d = new File(pp);
				d = new File(d, subdir);
				d = new File(d, getApplication().getPackageName());
				p = d.getAbsolutePath();
				if(!keys.contains(p)) {
					keys.add(p);
					labels.add("Other Dir:\n" + p);
				}
			}
			
		}
		
		// Load our current setting.
		p = mPreferences.getString(KEY_LOCAL_DEV, null);
		if(p != null) {
			if(!keys.contains(p)) {
				keys.add(p);
				labels.add("Current:\n"+p);
			}
		}
		
		lp.setEntryValues((CharSequence[])keys.toArray(new CharSequence[keys.size()]));
		lp.setEntries((CharSequence[])labels.toArray(new CharSequence[labels.size()]));
	}

	void populateRemoteDev(ListPreference lp) {
		if(accountMgr == null)
			// Activity just started; mgr was never prepped.
			return;
		
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				String accountName = mPreferences.getString(KEY_CREDENTIAL_NAME, null);
				if(accountName == null) {
					cancel(false);
				}
				else {
					try {
						List<String> files = ((GoogleDriveCloud)accountMgr).getPaths(authToken);
						Collections.sort(files);
						
	//					CharSequence[] ary = (CharSequence[])keys.toArray(new CharSequence[keys.size()]);
	//					lp.setEntries(ary);
	//					lp.setEntryValues(ary);
						Log.i("Pancorder", "Got files: " + files.toString());
					} catch (IOException e) {
						Log.e("Pancorder", "Can't list files", e);
					} catch (GoogleAuthException e) {
						Log.e("Pancorder", "Can't list files", e);
						Intent intent = ((UserRecoverableAuthException)e).getIntent();
						startActivity(intent);
					}
				}
				return null;
			}
		};
		task.execute();
	}
	
	/**
	 * Start an intent to prompt the user to choose the account to use with the
	 * selected cloud provider.
	 */
	void chooseAccount() {
		if(VAL_REMOTE_DEV_GOOGLE_DRIVE.equals(prefRemoteDev.getValue())) {
			// Google Drive
			accountMgr = new GoogleDriveCloud(this);
			accountMgr.authorizeWithGui(this, SAFR_CHOOSEACCOUNT_DONE);
		}
		else {
			// None.
			accountMgr = null;
			updateSummary_remote_dev();
		}
	}
	
	void chooseAccount_done(int resultCode, Intent data) {
		String accountName = null;
		Account account = null;
		
		if (resultCode == RESULT_OK && data != null) {
			authToken = accountMgr.getToken(data.getExtras());
			accountName = accountMgr.getAccountName(data.getExtras());
			account = GoogleAccountUtils.getGoogleAccountByName(getApplicationContext(), accountName);
		}
		
		if (account != null) {
			// Save selected account name.
			SharedPreferences.Editor editor = mPreferences.edit();
			editor.putString(KEY_CREDENTIAL_NAME, accountName);
			editor.commit();
		}
		else {
			// Canceled or failed; clear settings.
			accountMgr = null;
			prefRemoteDev.setValue(VAL_REMOTE_DEV_NONE);
		}

		// Update display.
		updateSummary_remote_dev();
	}

	/**
	 * Post descriptive text for Local Folder pref.
	 */
    void updateSummary_local_dev() {
    	String v = prefLocalDev.getValue();
        String s = getResources().getString(R.string.prefs__local_dev__summary_path);
        s = String.format(s, v);

        prefLocalDev.setSummary(s);
	}
	
	/**
	 * Post descriptive text for Cloud Provider pref.
	 */
	void updateSummary_remote_dev() {
        String v = prefRemoteDev.getValue();
        String s;
        
        if(v == null || VAL_REMOTE_DEV_NONE.equals(v)) {
	        // build summary string
	        s = getResources().getString(R.string.prefs__remote_dev__summary_none);
	        prefRemotePath.setEnabled(false);
        }
        else {
        	// translate value to text
	        int iDev = Arrays.asList(getResources().getStringArray(R.array.prefs__remote_dev__opt_keys)).indexOf(v);
	        String sDev = getResources().getStringArray(R.array.prefs__remote_dev__opt_labels)[iDev];
	
	        // get account name
	        String sName = mPreferences.getString(KEY_CREDENTIAL_NAME, "");
	        
	        // build summary string
	        s = getResources().getString(R.string.prefs__remote_dev__summary_service);
	        s = String.format(s, sDev, sName);
	        prefRemotePath.setEnabled(true);
	        
//	        populateRemoteDev(null);
        }
        prefRemoteDev.setSummary(s);
	}
	
	/**
	 * Post descriptive text for Remote Folder pref.
	 */
	void updateSummary_remote_path() {
		String v = prefRemotePath.getText();
        String s = getResources().getString(R.string.prefs__remote_path__summary_path);
        s = String.format(s, v);

        prefRemotePath.setSummary(s);
	}
}
