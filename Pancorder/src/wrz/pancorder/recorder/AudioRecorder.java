package wrz.pancorder.recorder;

import java.io.File;
import java.io.IOException;
import java.util.Date;


import android.media.MediaRecorder;
import android.media.MediaRecorder.OnErrorListener;
import android.media.MediaRecorder.OnInfoListener;
import android.util.Log;

/**
 * Wrapper for audio-only recorder.
 * 
 * @author William R. Zwicky
 */
public class AudioRecorder implements OnErrorListener, OnInfoListener {
	private static final String TAG = AudioRecorder.class.getSimpleName();

	/**
	 * Max duration of each recording segment, in ms.
	 */
	private static final int MAX_DURATION = 1000 * 60 * 5;

	private MediaRecorder mRecorder;
	private Recording mTarget;
	private long mStart = -1;
	

	/**
	 * Create recorder.
	 * 
	 * @param target where to write segments
	 */
	public AudioRecorder(Recording target) {
		this.mTarget = target;
	}
	
	public Recording getTarget() {
		return mTarget;
	}
	
	/**
	 * @return number of milliseconds we've been recording, or -1 if not
	 *         recording
	 */
	public long getSegmentDuration() {
		return mStart < 0 ? -1 : new Date().getTime() - mStart;
	}
	
	/**
	 * @return total length of all media in target dir, including whatever we're
	 *         currently recording
	 */
	public long getTotalLength() {
		//TODO finish!
		return getSegmentDuration();
	}

	/**
	 * Start recording audio into the target dir. Does nothing if already
	 * recording.
	 * 
	 * @throws IllegalStateException
	 *             if MediaRecorder was not configured properly
	 * @throws IOException
	 *             if MediaRecorder fails otherwise
	 */
	public void start() throws IllegalStateException, IOException {
		if(mRecorder != null)
			// already recording
			return;
		
		File file = mTarget.newSegment(".mp4");
		
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mRecorder.setAudioChannels(2);
        
        // AMR_WB seems quite good compared to AAC/64k.
        // Stereo doesn't work on my HTC Evo 3D.
        // Must call setOutputFormat before setAudioEncoder, but MPEG_4/AMR _does_ work.
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_WB);

        // Set maximum size per segment.
        // must be after setOutputFormat
        mRecorder.setMaxFileSize(1024 * 1024 * 100); //bytes
        mRecorder.setMaxDuration(MAX_DURATION);
        
		mRecorder.setOutputFile(file.getAbsolutePath());

		mRecorder.setOnErrorListener(this);
		mRecorder.setOnInfoListener(this);
		
		mRecorder.prepare();  //may throw
		mRecorder.start();		
		mStart = new Date().getTime();
	}

	/**
	 * Stop recorder (if running) and restart it on a new segment. If you just
	 * want to ensure the recorder is running, just call call start().
	 */
	public void restart() throws IllegalStateException, IOException {
		// stop/reset always wipes the MR, so we might as well create a new one.
		if(mRecorder != null)
			stop();
		start();
	}
	
	/**
	 * Stop recorder completely.  Does nothing if recorder is not running.
	 */
	public void stop() {
        if (mRecorder != null) {
        	// Stop and release the recorder
            mRecorder.stop();
            mRecorder.reset();  // bug in mp; must reset before release
            mRecorder.release();
            mRecorder = null;
            mStart = -1;
        }
    }

	public void onError(MediaRecorder mr, int what, int extra) {
		// MediaRecorder.MEDIA_RECORDER_ERROR_UNKNOWN
		// Error codes are useless, must assume all errors are fatal.
		Log.e(TAG, "MediaRecorder error #"+what+", extra="+extra);
		stop();
	}

	public void onInfo(MediaRecorder mr, int what, int extra) {
		Log.e(TAG, "MediaRecorder info #"+what+", extra="+extra);

		switch(what) {
		case MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED:
		case MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED:
			// recording MAY NOT be stopped when we receive these
			try {
				restart();
			} catch (Exception e) {
				Log.e(TAG, "Can't restart recording", e);
				stop();
			}
			break;
		default:
			stop();
			break;
		}
	}
}
