package wrz.pancorder.recorder;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import wrz.pancorder.java.MediaUtil;



/**
 * Metadata for a single recording.
 * 
 * @author William R. Zwicky
 */
public class Recording {
	protected static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);
	private static final String ACTIVE_MARKER = ".active";
	
	/** Directory holding recording(s). */
	protected File location = null;
	/** Millisecond of start of recording, as from Date.getTime(). */
	protected long start = -1;
	/** Nominal length of recording (cache for user). */
	protected long duration = -1;


	public Recording(File location) {
		this.location = location;
	}
	
	public File getDir() {
		return location;
	}

	/** Millisecond of start of recording, as from Date.getTime(). */
	public long getStart() {
		return this.start;
	}
	
	/**
	 * @return current duration value, in ms. Call findSpan() to compute this.
	 */
	public long getDuration() {
		return duration;
	}

	/**
	 * Change the duration value. This has no effect on the recording itself, it
	 * only changes the stored length value.
	 * 
	 * @param duration
	 *            nomainal length of recording, in milliseconds
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}
	
	/**
	 * @return true if recorder should be recording to this file
	 */
	public boolean isActive() {
		if(location == null)
			return false;
		else {
			if(location.isFile())
				return location.getName().endsWith(".recording");
			else
				return new File(location, ACTIVE_MARKER).exists();
		}
	}

	/**
	 * Mark this recording as being the one we should record to.
	 * 
	 * @param active
	 */
	public void setActive(boolean active) {
		File f = new File(location, ACTIVE_MARKER);
		if(active)
			try {
				f.createNewFile();
			} catch (IOException e) {
				// ignore; what can we do?
			}
		else
			f.delete();
	}
	
	/**
	 * Compute the span of time that this recording covers. Use getStart() and
	 * getDuration() to retreive results. Sets both to -1 if unable to compute
	 * starting time.
	 */
	public void findSpan() {
		if(location.exists()) {
			if(location.isFile()) {
				duration = MediaUtil.getDuration(location);
				start = location.lastModified();
				if(duration > 0)
					start -= duration;
			}
			else {
				// earliest time recorded in any segment
				long begin = location.lastModified();

				// latest time recorded in any segment
				long end = -1;
				File[] files = location.listFiles();
				for(File f : files) {
					end = Math.max(end, f.lastModified());
					try {
						// hopefully we can parse file name for the definitive start
						begin = Math.min(begin, DATE_FORMAT.parse(f.getName()).getTime());
					} catch (ParseException e) {
						// else compute media length to determine start
						long len = MediaUtil.getDuration(location);
						begin = Math.min(begin, f.lastModified() - len);
					}
				}
				
				if(begin == Long.MAX_VALUE) {
					// can't determine start, so can't determine len
					start = -1;
					duration = -1;
				}
				else {
					start = begin;
					duration = end - begin;
				}
			}
		}
		else {
			start = -1;
			duration = -1;
		}
	}

	/**
	 * WARNING: Can only generate one new base name per second. If you need
	 * several names at once, use different suffixes.
	 * 
	 * @param suffix
	 *            file extension, like ".mp4" or ".csv"
	 * @return path and name of unique, non-existent file in recording dir
	 */
	public File newSegment(String suffix) {
		String now = DATE_FORMAT.format(new Date());
		return new File(getDir(), now+suffix);
	}
}
