package wrz.pancorder.recorder;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;



/**
 * Helpers for managing collections of recordings.
 * 
 * @author William R. Zwicky
 */
public class Recordings {
	protected static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);

	protected File recordingDir;

	/**
	 * @param dir directory where recordings are stored
	 */
	public Recordings(File dir) {
		this.recordingDir = dir;
	}
	
	/**
	 * Determine if we should be recording to a file, and return it's location.
	 * If multiple files are so flagged, they are cleaned up.
	 * 
	 * @return path and name that we should be recording to
	 */
	public Recording getActive() {
		if(recordingDir == null || !recordingDir.exists())
			return null;
		else {
			File[] files = recordingDir.listFiles();
			if(files == null || files.length == 0)
				return null;
			else {
				ArrayList<Recording> candidates = new ArrayList<Recording>();
				for(File file : files) {
					try {
						Recording r = get(file);
						if(r != null && r.isActive())
							candidates.add(r);
					} catch (FileNotFoundException e) {
						// impossible; ignore
					}
				}

				switch(candidates.size()) {
					case 0:
						// no recordings active
						return null;
					case 1:
						// only one active
						return candidates.get(0);
					default: {
						// Sort by start time.
						Collections.sort(candidates, new Comparator<Recording>() {
							public int compare(Recording lhs, Recording rhs) {
								// don't need to worry about extension
								return lhs.location.getName().compareTo(rhs.location.getName());
							}
						});
						
						// Extract the last element as the active recording.
						Recording active = candidates.remove(candidates.size()-1);
						return active;
					}
				}
			}
		}
	}

	/**
	 * Return all finished (non-active) recordings. You should call getActive()
	 * first to clean up the database and ensure this list is complete.
	 * 
	 * @return List of finished recordings in database, sorted by most recent
	 */
	public List<Recording> getClosed() {
		ArrayList<Recording> recs = new ArrayList<Recording>();

		if(recordingDir == null || !recordingDir.exists())
			; //ignore
		else {
			File[] files = recordingDir.listFiles();
			if(files == null || files.length == 0)
				; //ignore
			else {
				for(File file : files) {
					try {
						Recording r = get(file);
						if(r != null && !r.isActive())
							recs.add(r);
					} catch (FileNotFoundException e) {
						// impossible; ignore
					}
				}
				Collections.sort(recs, new Comparator<Recording>() {
					public int compare(Recording lhs, Recording rhs) {
						// sort recent recordings to the top
						return lhs.start < rhs.start ? +1
							: lhs.start > rhs.start ? -1
							: 0;
					}
				});
			}
		}

		return recs;
	}

	/**
	 * Get record corresponding to a file. Only returns file and nominal start
	 * time if it can figure it out, but not duration due to amount of time
	 * required to compute it. Call load(rec) for proper start and duration.
	 * 
	 * @param file
	 *            recording to find
	 * 
	 * @return easy-to-compute metadata for file, or null if file exists but is
	 *         not a recording
	 * @throws FileNotFoundException
	 *             if file does not exist
	 */
	public Recording get(File file) throws FileNotFoundException {
		if(!file.exists())
			throw new FileNotFoundException(file.getAbsolutePath());
		if(!file.isDirectory() || file.length() == 0)
			return null;

		Recording r = new Recording(file);
	
		try {
			// parses what it can, ignores the rest
			r.start = DATE_FORMAT.parse(file.getName()).getTime();
		} catch (ParseException e) {
			r.start = -1;
		}
		
		return r;
	}

	/**
	 * Finish populating a data record.
	 * 
	 * @param rec with start and duration filled in
	 */
	public void load(Recording rec) {
		rec.findSpan();
	}

	/**
	 * Create new recording starting now.
	 * 
	 * @return new recording object
	 */
	public Recording create() {
		Recording r = null;

		long now = new Date().getTime();
		File f = new File(recordingDir, DATE_FORMAT.format(now));
		if(f.mkdirs()) {
			r = new Recording(f);
			r.start = now;
		}

		return r;
	}
}
