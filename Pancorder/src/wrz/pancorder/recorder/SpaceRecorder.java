package wrz.pancorder.recorder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import wrz.pancorder.location.LastLocationFinder;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

/**
 * Query and write sensor data to a data file.
 * 
 * @author William R. Zwicky
 */
public class SpaceRecorder {
	private static final String TAG = SpaceRecorder.class.getSimpleName();

	/**
	 * Min time between location updates.  May be longer.
	 * Prior to Jellybean, the minTime parameter was only a hint.
	 */
	private static final long DELTA_TIME = 1000 * 60 * 5;	// ms; docs recommend 5 mins
	/**
	 * Min distance between location updates. Note that *both* time and distance
	 * minimums must be met.
	 */
	private static final float DELTA_DIST = 100;			// m; i say 100m
	/**
	 * Time delay between polls of the other sensors.
	 */
	private static final int SENSOR_DELAY = 1000000 * 30;	// microseconds
	
	private Context mContext;
	private Recording mTarget;

	private LocationManager mLocationManager;
	private LocationListener mLocationListener;
	private SensorManager mSensorManager;
	private SensorEventListener mSensorListener;

	private boolean has_loc = false;
	private Location cur_loc;
	private boolean has_light = false;
	private float cur_light;
	private boolean has_accel = false;
	private float[] cur_accel = new float[3];
	private boolean has_magnet = false;
	private float[] cur_magnet = new float[3];
	private boolean has_pressure = false;
	private float cur_pressure;
	private boolean has_temp = false;
	private float cur_temp;

	private PrintWriter mOut;


	/**
	 * Create recorder.
	 * 
	 * @param context provides access to system services
	 * @param target where to write data
	 */
	// recommend app context, not activity or service
	public SpaceRecorder(Context context, Recording target) {
		this.mContext = context;
		this.mTarget = target;
		

		cur_loc = new LastLocationFinder(mContext).getLastBestLocation();
		if(cur_loc != null)
			has_loc = true;
	}

	/**
	 * Start recording now. Works in background, whether calling thread runs or
	 * not.
	 * 
	 * @throws IOException
	 *             if unable to create output file
	 */
	public void start() throws IOException {
		// open file first so exceptions skip code below
		open();

		Criteria crit = new Criteria();
		crit.setPowerRequirement(Criteria.POWER_LOW);
		crit.setSpeedAccuracy(Criteria.ACCURACY_HIGH);

		mLocationListener = new LocationListener() {
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}

			public void onLocationChanged(Location location) {
				cur_loc = location;
				has_loc = true;
				write();
			}
		};

		// Register a location listener.
		mLocationManager = (LocationManager) mContext
				.getSystemService(Context.LOCATION_SERVICE);
		mLocationManager.requestLocationUpdates(DELTA_TIME, DELTA_DIST, crit,
				mLocationListener, null);
		
		// --------

		// Sensors explained at:
		//   https://groups.google.com/forum/#!topic/android-developers/GOm9yhTFZaM
		// copied here:
		//   http://stackoverflow.com/questions/7858759/android-type-linear-acceleration-sensor-what-does-it-show

		mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
		
		mSensorListener = new SensorEventListener() {
			public void onSensorChanged(SensorEvent event) {
				switch(event.sensor.getType()) {
				case Sensor.TYPE_ACCELEROMETER:  // raw sensor
					// All values are in SI units (m/s^2) 
					// values[0]: Acceleration minus Gx on the x-axis 
					// values[1]: Acceleration minus Gy on the y-axis 
					// values[2]: Acceleration minus Gz on the z-axis 
					has_accel = true;
					cur_accel[0] = event.values[0];
					cur_accel[1] = event.values[1];
					cur_accel[2] = event.values[2];
					break;
				//case Sensor.TYPE_AMBIENT_TEMPERATURE:  //API 14
					// values[0]: ambient (room) temperature in degree Celsius.
				//case Sensor.TYPE_GRAVITY:  //"fused", not well defined
				case Sensor.TYPE_GYROSCOPE:  // raw sensor
					// All values are in radians/second
					// values[0]: Angular speed around the x-axis 
					// values[1]: Angular speed around the y-axis 
					// values[2]: Angular speed around the z-axis 
					//-ignored
					break;
				case Sensor.TYPE_LIGHT:  // raw sensor
					// values[0]: Ambient light level in SI lux units 
					has_light = true;
					cur_light = event.values[0];
					break;
				//case Sensor.TYPE_LINEAR_ACCELERATION:  //"fused", not well defined
				case Sensor.TYPE_MAGNETIC_FIELD:  //?
					// All values are in micro-Tesla (uT) and measure the ambient magnetic field in the X, Y and Z axis.
					has_magnet = true;
					cur_magnet[0] = event.values[0];
					cur_magnet[1] = event.values[1];
					cur_magnet[2] = event.values[2];
					break;
				//case Sensor.TYPE_ORIENTATION:  //deprecated
				case Sensor.TYPE_PRESSURE:  // raw sensor
					// values[0]: Atmospheric pressure in hPa (millibar) 
					has_pressure = true;
					cur_pressure = event.values[0];
					break;
				//case Sensor.TYPE_PROXIMITY:  //not used here
					// values[0]: Proximity sensor distance measured in centimeters
				//case Sensor.TYPE_RELATIVE_HUMIDITY:  //API 14
					// values[0]: Relative ambient air humidity in percent
				//case Sensor.TYPE_ROTATION_VECTOR:  //"fused", not well defined
				case Sensor.TYPE_TEMPERATURE:  // raw sensor
					// Deprecated in API 14 in favor of TYPE_AMBIENT_TEMPERATURE
					// values[0]: ambient (room) temperature in degree Celsius.
					has_temp = true;
					cur_temp = event.values[0];
					break;
				default:
					// ignore all other sensors
				}
			}
			
			public void onAccuracyChanged(Sensor sensor, int accuracy) {
			}
		};

		List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
		for(Sensor s : sensors) {
			switch(s.getType()) {
			case Sensor.TYPE_ACCELEROMETER:
			case Sensor.TYPE_LIGHT:
			case Sensor.TYPE_MAGNETIC_FIELD:
			case Sensor.TYPE_PRESSURE:
			case Sensor.TYPE_TEMPERATURE:
				  mSensorManager.registerListener(mSensorListener, s, SENSOR_DELAY);
				  break;
			  default:
				  // ignore
				  break;
			}
		}
		
		write();
	}

	/**
	 * Stop recording, close files, release all resources.
	 */
	public void stop() {
		mLocationManager.removeUpdates(mLocationListener);
		mSensorManager.unregisterListener(mSensorListener);
		close();
	}


	/**
	 * Open output file for appending.
	 * 
	 * @throws IOException
	 *             if unable to create output file
	 */
	private void open() throws IOException {
		File file = new File(mTarget.getDir(), "sensors.csv");
		boolean exists = file.exists();
		mOut = new PrintWriter(new FileWriter(file, true), true);
		if(!exists)
			writeHeader();
	}

	/**
	 * Close output file, release file-related resources.
	 */
	private void close() {
		if (mOut != null) {
			mOut.close();
			mOut = null;
		}
	}

	/**
	 * Write CSV header line.  MUST STAY IN SYNC WITH write()!
	 */
	private void writeHeader() {
		// Nominal time for this record.
		mOut.print("time:ms");			//from Jan 1, 1970
		
		// Location at time.
		mOut.print(',');
		mOut.print("latitide:deg");		//
		mOut.print(',');
		mOut.print("longitude:deg");	//
		mOut.print(',');
		mOut.print("accuracy:m/1sd");	//1 std dev; nullable
		mOut.print(',');
		mOut.print("altitude:m");		//above sea level; nullable
		mOut.print(',');
		mOut.print("speed:m/s");		//nullable

		// Sensor values at time.
		mOut.print(',');
		mOut.print("light:lux");			//nullable
		mOut.print(',');
		mOut.print("temperature:deg_C");	//nullable
		mOut.print(',');
		mOut.print("pressure:millibar");	//nullable
		mOut.print(',');
		mOut.print("accel_x:m/s^2");		//nullable
		mOut.print(',');
		mOut.print("accel_y:m/s^2");		//nullable
		mOut.print(',');
		mOut.print("accel_z:m/s^2");		//nullable
		mOut.print(',');
		mOut.print("magnet_x:microtesla");	//nullable
		mOut.print(',');
		mOut.print("magnet_y:microtesla");	//nullable
		mOut.print(',');
		mOut.print("magnet_z:microtesla");	//nullable
		
		mOut.println();
	}

	/**
	 * Write CSV data line.  MUST STAY IN SYNC WITH writeHeader()!
	 */
	private void write() {
		if(mOut == null) {
			Log.e(TAG, "SpaceRecorder.write() called after close()");
			return;
		}
		
		if(has_loc) {
			// Nominal time for this record.
			mOut.print(cur_loc.getTime());
	
			// Most recent location.
			mOut.print(',');
			mOut.print(cur_loc.getLatitude());
			mOut.print(',');
			mOut.print(cur_loc.getLongitude());
			mOut.print(',');
			mOut.print(cur_loc.hasAccuracy() ? cur_loc.getAccuracy() : "");
			mOut.print(',');
			mOut.print(cur_loc.hasAltitude() ? cur_loc.getAltitude() : "");
			mOut.print(',');
			mOut.print(cur_loc.hasSpeed() ? cur_loc.getSpeed() : "");
		}
		else {
			mOut.print(new Date().getTime());
			mOut.print(',');
			mOut.print(',');
			mOut.print(',');
			mOut.print(',');
			mOut.print(',');
		}

		// Most recent sensor values.
		mOut.print(',');
		mOut.print(has_light ? cur_light : "");
		mOut.print(',');
		mOut.print(has_temp ? cur_temp : "");
		mOut.print(',');
		mOut.print(has_pressure ? cur_pressure : "");
		mOut.print(',');
		mOut.print(has_accel ? String.format("%f,%f,%f", cur_accel[0], cur_accel[1], cur_accel[2]) : "");
		mOut.print(',');
		mOut.print(has_magnet ? String.format("%f,%f,%f", cur_magnet[0], cur_magnet[1], cur_magnet[2]) : "");

		mOut.println();
	}
}
