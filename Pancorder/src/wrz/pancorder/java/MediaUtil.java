package wrz.pancorder.java;

import java.io.File;
import java.io.IOException;

import android.media.MediaPlayer;
import android.util.Log;

public class MediaUtil {
	/**
	 * Determine the length of a media file. Uses Android's MediaPlayer class.
	 * 
	 * @param mediaFile path and name of file to examine
	 * @return length of media inside file, in milliseconds; or -1 if cannot find or parse
	 */
	static public long getDuration(File mediaFile) {
		if(mediaFile.exists()) {
			MediaPlayer mp = new MediaPlayer();
			
			try {
				mp.setDataSource(mediaFile.getAbsolutePath());
				mp.prepare();
				return mp.getDuration();
			} catch(IOException e) {
				// ignore; not a media file
				return -1;
			} catch (Exception e) {
				Log.e("Pancorder", "Can't understand file "+mediaFile.getAbsolutePath(), e);
				return -1;
			}
			finally {
				mp.reset();  // bug in mp; must reset before release
				mp.release();
				mp = null;
			}
		}
		else
			return 0;
	}
}
