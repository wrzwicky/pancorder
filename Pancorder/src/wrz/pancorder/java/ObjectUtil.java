package wrz.pancorder.java;

public class ObjectUtil {

	/** Checks whether two providers are the same */
	static public boolean equals(String s1, String s2) {
		if (s1 == null)
			return s2 == null;
		else
			return s1.equals(s2);
	}

}
