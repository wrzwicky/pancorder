package wrz.pancorder.location;

import java.util.List;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

/**
 * Derived from http://code.google.com/p/android-protips-location/ which does
 * too much work, and provides two identical implementations.
 * 
 * @author William R. Zwicky
 */
public class LastLocationFinder {
	protected Context context;
	protected LocationManager locationManager;

	public LastLocationFinder(Context context) {
		this.context = context;
		locationManager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
	}

	/**
	 * @return The most accurate and / or timely previously detected location.
	 */
	public Location getLastBestLocation() {
		Location bestResult = null;
		float bestAccuracy = Float.MAX_VALUE;
		long bestTime = Long.MAX_VALUE;

		// Iterate through all the providers on the system, keeping
		// note of the most accurate result within the acceptable time limit.
		// If no result is found within maxTime, return the newest Location.
		List<String> matchingProviders = locationManager.getAllProviders();
		for (String provider : matchingProviders) {
			Location location = locationManager.getLastKnownLocation(provider);
			if (location != null) {
				float accuracy = location.getAccuracy();
				long time = location.getTime();

				if ((accuracy < bestAccuracy)) {
					bestResult = location;
					bestAccuracy = accuracy;
					bestTime = time;
				} else if (bestAccuracy == Float.MAX_VALUE && time < bestTime) {
					bestResult = location;
					bestTime = time;
				}
			}
		}

		return bestResult;
	}
}
