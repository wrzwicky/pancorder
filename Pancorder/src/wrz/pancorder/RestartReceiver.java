package wrz.pancorder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

/**
 * Restart the recorder when certain system events happen. Events are listed in
 * manifest.  Note that as of Android 3.1, we can no longer receive our own PACKAGE events.
 * <P>
 * Events are: system reboot,  app is installed.
 * 
 * @see http://stackoverflow.com/questions/8515310/broadcast-receiver-for-package-added-not-working-from-android-3-1-onwards
 * 
 * @author William R. Zwicky
 */
public class RestartReceiver extends BroadcastReceiver {
	private static final String TAG = BroadcastReceiver.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
        Uri d = intent.getData();
        
        boolean go = false;
        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
        	go = true;
        else if(intent.getAction().startsWith("android.intent.action.PACKAGE_")) {
            // Only run if our app is the thing that was installed.
            if(d.getScheme().equals("package") && d.getSchemeSpecificPart().equals("wrz.pancorder"))
            	go = true;
        }
        
        if(go) {
        	Log.i(TAG, "Restarting active Pancording due to: "+intent);
        	RecorderServiceCmd.restart(context);
        }
	}
}
