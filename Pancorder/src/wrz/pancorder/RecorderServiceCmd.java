package wrz.pancorder;

import wrz.pancorder.recorder.Recording;
import android.content.Context;
import android.content.Intent;

public class RecorderServiceCmd {
	/**
	 * Tell service to resume if any recording is marked active.
	 * 
	 * @param context
	 */
	static public void restart(Context context) {
		Intent serviceIntent = new Intent(context, RecorderService.class);
		serviceIntent.putExtra("command", "restart");
		context.startService(serviceIntent);
	}

	/**
	 * Tell service to start recording to the given target.
	 * 
	 * @param context
	 * @param target
	 */
	static public void start(Context context, Recording target) {
		Intent serviceIntent = new Intent(context, RecorderService.class);
		serviceIntent.putExtra("command", "start");
		serviceIntent.putExtra("file", target.getDir());
		context.startService(serviceIntent);
	}

	/**
	 * Tell service to stop whatever it's currently doing. Note that the current
	 * recording is NOT un-marked.
	 * 
	 * @param context
	 */
	static public void stop(Context context) {
		Intent serviceIntent = new Intent(context, RecorderService.class);
		serviceIntent.putExtra("command", "stop");
		context.startService(serviceIntent);
	}

	/**
	 * @return the length of the current recording. Only valid if recorder is
	 *         actually running.
	 */
	static public long getTotalLength() {
		return RecorderService.getInstance() == null ? 0 : RecorderService.getInstance().getTotalLength();
	}
}
